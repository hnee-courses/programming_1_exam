# Tree Volume Estimation
Exam assignment for the course "programming I" at Forest Information Technology, Eberswalde University for Sustainable Development

This library shows some ways to calculate tree stem volume from  

## Installation 
This guide is tested on ubuntu mate linux 20.04

### environment setup
Setup an environment called "programming_2_exam". If it is already active existing deactivate and delete it.
```shell
conda deactivate
conda env remove -n programming_1_exam 
```

Now we create the environment using conda. Then activate it.
```shell
conda create --name programming_1_exam -y python=3.9
conda activate programming_1_exam
```

Optionally add the interpreter to PyCharm for later use. Use the add interpreter dialogue to do so.
![img.png](doc/python_interpreter.png)


Run the code now.
```shell
pip install --editable .

```

You can run the unit tests to check if everything is allright.
```shell
# run the tests
pytest
```

### Run the program via cli
```shell
python3 treevolume/main.py
```
If this fails because the module cannot be found try adding the current directory to the PYTHONPATH
```shell    
PYTHONPATH=$PYTHONPATH:./ python3 treevolume/main.py
```
If this does not work either, something weird is going on. Then go to the "deployment setup" guide below

It will output this prompt.
![doc/img.png](doc/cli_first_command.png)

To calculate the volumes all parameters could be passed to the cli or they will be prompted individually:

```shell
# get guided through volume calculation
python3 treevolume/main.py volume 
# calculate the volume using equation 102 from zianis et.al. but get prompted for dbh and height
python3 treevolume/main.py volume --f_name=eq102
# calculate the volume using equation 102 from zianis et.al. in one go
python3 treevolume/main.py volume --f_name=eq102 --dbh=10 --height=20
# calculate the volume using equation 170 from zianis et.al. in one go
python3 treevolume/main.py volume --f_name=eq170 --dbh=10 --height=20
# denzin
python3 treevolume/main.py volume --f_name=denzin --dbh=10 --height=20
# huber
python3 treevolume/main.py volume --f_name=huber --dbh=10 --height=20
```

Additionally you can estimate the diamter at arbitrary heights
```shell
# get guided through volume calculation, i.e.
python3 treevolume/main.py diameter --total_height 30 --diameter_height=10 --dbh=10 
```

To reproduce the analysis in the report run the analysis function
```shell
python3 treevolume/main.py volume-evaluation-run
```


### Use the library from within python
run an equation 
```python
from treevolume.functions import DataHelper, TreeVolumePredictor
TreeVolumePredictor.zianis_eq_102(D=0.30, H=40)
```

run a comparison and save the result to /tmp/treevolume_analysis.json
```python
from treevolume import compare_functions
compare_functions.whole_analysis(output_filename="/tmp/treevolume_analysis.json")
```


## deployment setup
if you want a simple shippable package you can install you can build it by 
```shell
# build a source package
python3 setup.py sdist 
```

```shell
python3 setup.py bdist_wheel
pip install dist/Tree_Steam_Volume_Calculator-0.0.4-py3-none-any.whl
```

Now you can do everything as you did above there, but replace "python3 treevolume/main.py" with "dendrometry".


```shell
#fully interactive way
dendrometry

# just everything
dendrometry diameter --total_height 30 --diameter_height=10 --dbh=10 

```







