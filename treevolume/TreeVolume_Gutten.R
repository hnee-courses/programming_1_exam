##
# get guttenberg, 1915 data from R package
# https://rdrr.io/cran/FAwR/
# see https://link.springer.com/book/10.1007/978-1-4419-7762-5 - DOI: 10.1007/978-1-4419-7762-5
## 

# install the package
install.packages("FAwR")

# load the package
library(FAwR)

# load the data
data(gutten)

# write the guttenberg data to a csv ignoring the index
write.csv(gutten, "gutten.csv", row.names = FALSE)
