"""
Command line demo tool

@author: Christian Winkelmann
"""
from treevolume.compare_functions import whole_analysis
from treevolume.functions import TreeVolumePredictor
import click


@click.group()
def cli():
    pass


@cli.command()
@click.option(
    "--f_name",
    default="denzin",
    prompt='name of the tree volume calculation. select "denzin", "huber", "eq102" or "eq170" ',
    type=click.Choice(["denzin", "huber", "eq102", "eq170"], case_sensitive=True),
    help="Selector of functions, which predict the volume of the log.",
)
@click.option(
    "--dbh",
    prompt="diameter at breast height [cm]",
    help="The diameter at breast height in centimeters",
    type=click.FLOAT,
)
@click.option(
    "--height",
    prompt="total tree height [m]",
    help="Total tree height in meter",
    type=click.FLOAT,
)
def volume(f_name, dbh, height):
    """
    the tree stem volume
    """
    H = height

    if f_name == "denzin":
        v = TreeVolumePredictor.denzin(dbh=dbh, h=H)

    if f_name == "huber":
        h = H / 2
        d_2 = TreeVolumePredictor.taper_spangberg(H=H, h=h, dbub=dbh)
        d_2 = d_2 / 100  # get it to meter
        v = TreeVolumePredictor.huber(d_2=d_2, h=h)

        print(f"It is estimated it has a diameter at mid height of {d_2}")

    if f_name == "eq102":
        v = TreeVolumePredictor.zianis_eq_102(H=H, D=dbh)

    if f_name == "eq170":
        v = TreeVolumePredictor.zianis_eq_170(H=H, D=dbh)

    print(f"The volume of the tree is {round(v,6)} cubic meter.")


@cli.command()
@click.option(
    "--total_height",
    prompt="total tree height [m]",
    help="Total tree height in meter",
    type=click.FLOAT,
)
@click.option(
    "--diameter_height",
    prompt="height X [m] for unknown diameter",
    help="height X [m] for unknown diameter",
    type=click.FLOAT,
)
@click.option(
    "--dbh",
    prompt="diameter [cm] at breast height",
    help="The diameter at breast height in centimeters",
    type=click.FLOAT,
)
def diameter(dbh, diameter_height, total_height):
    """
    tree stem diameter at a specific height
    """
    h = diameter_height
    d = TreeVolumePredictor.taper_spangberg(H=total_height, h=h, dbub=dbh)
    print(f"The diameter of the tree at {round(h, 2)}m is {round(d, 2)}cm.")


@cli.command()
def volume_evaluation_run():
    """
    tree stem diameter at a specific height
    """
    whole_analysis(output_filename="./treevolume_analysis.json")


if __name__ == "__main__":
    cli.add_command(volume)
    cli.add_command(diameter)
    cli.add_command(volume_evaluation_run)

    cli()
