"""
Collection of functions to calculate dendrometrics values

@author: Christian Winkelmann
"""

import math
import os
from pathlib import Path
import pandas as pd


class TreeVolumePredictor(object):
    """
    collection of functions to calculate tree volumes.
    """

    @staticmethod
    def cylinder(H: float, D: float) -> float:
        """
        Reference formula which assumes a tree is a perfect cylinder

        :param D: diameter at breast height in meters
        :param H: height in meters
        :return v: the volume of the tree in cubic meters
        """
        r = D / 2
        v = math.pi * (r**2) * H
        return v

    @staticmethod
    def denzin(dbh: float, h: float) -> float:
        """
        Tree volume prediction based on (Denzin, 1915) in cubic meters
        :param dbh: diameter at breast height in centi-meters
        :param h: height in meters
        :return: the volume of the tree in cubic meters
        """
        if dbh is None or h is None:
            return None
        # dbh = dbh * 100  # Denzin is centimeters instead of meters
        v = (dbh**2 / 1000) * (1 + 0.03 * (h - 30))
        return v

    @staticmethod
    def huber(d_2: float, h: float) -> float:
        """
        Tree volume prediction based on huber in cubic meters
        :param d_2: diameter at height / 2 in centimeters
        :param h: height in meters
        :return: the volume of the tree in cubic meters
        """
        return TreeVolumePredictor.cylinder(D=d_2, H=h)

    @staticmethod
    def taper_populus_tremuloides(dbh: float, H: float, h: float) -> float:
        """
        calculate diameter at height h, for a tree of total height H and diameter at breast height dbh

        From salekin - 2021 - "global tree taper modelling (8)"
        :param h:
        :param dbh:
        :param H:
        """

        b1 = 0.3087
        b2 = 0.5364
        BH = 1.3  # Breast height
        d_h = b1 * dbh * ((H - h) / (H - BH)) ** b2

        return d_h

    @staticmethod
    def taper_spangberg(H: float, h: float, dbub: float, species="spruce") -> float:
        """
        calculate diameter at height h, for a tree of total height H and diameter at breast height dbh

        see Taper functions for Picea abies (L.) Karst. and Pinus sylvestris L. in Sweden - Kalle Spångberg, John Arlinger, Lars Wilhelmsson, SkogForsk Sven-Olof Lundqvist and Örjan Hedenberg, STFI

        :param h: height at which diamter is calculated
        :param dbub: diameter under bark
        :param H: total tree height
        :type dbh: object
        """
        if species == "spruce":  # picea abies
            b1 = -3.8158
            b2 = 1.8128
            b3 = -2.0333
            b4 = 254.4
            a1 = 0.6778
            a2 = 0.0677
        else:  # pine
            b1 = -5.6198
            b2 = 2.7310
            b3 = -3.1194
            b4 = 141.9
            a1 = 0.7611
            a2 = 0.0757

        x = h / H
        I1 = 1 if x <= a1 else 0
        I2 = 1 if x <= a2 else 0

        d = math.sqrt(
            dbub**2
            * (
                (
                    b1 * (x - 1)
                    + b2 * (x**2 - 1)
                    + b3 * (a1 - x) ** 2 * I1
                    + b4 * (a2 - x) ** 2 * I2
                )
            )
        )

        return d

    @staticmethod
    def zianis_eq_86(H: float, D: float) -> float:
        """
        volume for Picea abies
        :param H: Height in meter
        :param D Diameter in centimeter:
        :return: volume in cubic meter
        """

        a = 0.00011261
        b = 0.87852
        v = a * (H * D**2) ** b
        return v

    @staticmethod
    def zianis_eq_94(H: float, D: float) -> float:
        """
        volume for Picea abies
        H height in meters
        D Diameter in meters
        :return Volume in cubic meters
        """
        a = 0.502
        # a = 0.418 equation 95
        return a * H * D**2

    @staticmethod
    def zianis_eq_102(H: float, D: float) -> float:
        """
        volume for Norway Spruce (Picea abies)
        :param H: Height in meter
        :param D Diameter in centimeter:
        :return: volume in cubic meter
        """

        v = (
            0.6844
            * H**3.0296
            * (D**2.0560)
            * ((H - 1.3) ** -1.7377)
            * (D + 40) ** -0.9756
        )
        return v / 1000

    @staticmethod
    def zianis_eq_170(H: float, D: float) -> float:
        """
        volume for Scots pine (Pinus sylvestris)
        :param H: Height in meter
        :param D Diameter in centimeter:
        :return: volume in cubic meter

        """

        v = 0.1028 * D**2 + 0.02705 * D**2 * H + 0.005215 * D * H**2
        return v / 1000

    @staticmethod
    def zianis_eq_195(H: float, D: float) -> float:
        """
        volume for Douglas Fir (Pseudotsuga menziesii)

        :param H: Height in meter
        :param D Diameter in centimeter:
        :return: volume in cubic meter
        """

        a = 1.8211
        b = 4.153
        c = 2.1342
        d = -2.6902
        e = -1.4265
        v = a * H**b * D**c * (H - 1.3) ** d * (D + 40) ** e
        return v / 1000


class UnitHelper:
    """
    helper Class to convert some unit others
    """

    foot_meter_ratio = 0.3048
    inch_meter_ratio = 0.0254

    def foot_to_meter(self, l: float) -> float:
        """
        :param l: length in feet
        :return: length in meter
        """
        return l * self.foot_meter_ratio

    def meter_to_foot(self, l: float) -> float:
        """
        :param l:
        :return:
        """
        return l / self.foot_meter_ratio

    def inch_to_meter(self, l: float) -> float:
        """
        :param l: length in inch
        :return: length in meter
        """
        return l * self.inch_meter_ratio

    def meter_to_inch(self, l: float) -> float:
        """
        :param l: length in inch
        :return: length in meter
        """
        return l / self.inch_meter_ratio


class DataHelper(object):
    """
    Helper Class to retrieve dataset
    """

    gutten_path = Path(os.path.abspath(__file__)).parent.joinpath(
        "data/gutten/gutten.csv"
    )
    schepaschenko2017_path = Path(os.path.abspath(__file__)).parent.joinpath(
        "data/schepaschenko2017/Biomass_tree_DB.xlsx"
    )

    def __init__(self):
        # print(self.schepaschenko2017_path)
        # print(os.listdir(self.schepaschenko2017_path.parent))
        pass

    def get_gutten_df(self) -> pd.DataFrame:
        """
        return the guttenberg 1915 dataset but all in they base SI unit.
        For the original dataset see
        :return:
        """
        with open(self.gutten_path, "r") as f:
            return pd.read_csv(f)

    def get_schepaschenko2017_df(self) -> dict:
        """
        reads the schepaschenko data and returns a list of 4 dataframes.

        See https://doi.pangaea.de/10.1594/PANGAEA.871491 for details

        :return:
        """
        with open(self.schepaschenko2017_path, "r") as f:
            xl_file = pd.ExcelFile(self.schepaschenko2017_path)
            dfs = {
                sheet_name: xl_file.parse(sheet_name)
                for sheet_name in xl_file.sheet_names
            }

            return dfs
