"""
Test Class for different used functions to ensure all work as they are exptected

@author: Christian Winkelmann
"""
import unittest

from treevolume.functions import TreeVolumePredictor, DataHelper


class TreeVolumePredictorTests(unittest.TestCase):
    def setUp(self):
        self.tve = {
            "metadata": {
                "dbh": {
                    "description": "diameter at breast height (1.3m) in centimeters "
                },
                "age": {"description": "Tree Age in years "},
                "h": {"description": "Tree height in meters"},
                "v": {"description": "Volume of tree in cubic decimeters"},
            },
            "data": [
                {"dbh": 26.5, "age": 100, "h": 30, "v": 928.0},
            ],
        }
        # self.longMessage = True  # enable long message at testing
        self.dh = DataHelper()

    def tearDown(self):
        """
        tear down anything we have setup.
        :return:
        """
        pass

    def test_cylinder(self):
        tve = self.tve["data"][0]
        h = tve["h"]
        dbh = tve["dbh"] / 100  # diameter to meter
        v_expected = tve.get("v")
        v_expected /= 1000  # the data provided by gutten is in cubic centimeters
        v_actual = TreeVolumePredictor.cylinder(H=h, D=dbh)
        ape = self.absolute_percent_error(v_actual, v_expected)

        # we are expecting less then 30 percent difference from the predicted volume to the real volume
        self.assertLessEqual(
            ape,
            0.8,
            f"the absolute percentage error is {ape}, \n"
            f"expected volume was {v_expected}m^3 and actual volume is {v_actual} m^3",
        )

    def test_taper(self):
        """test the diameter calculation"""
        tve = self.tve["data"][0]
        H = tve["h"]
        h = H / 2
        dbh = tve["dbh"]
        # if centimeter goes in, centimeter goes out
        d_2 = TreeVolumePredictor.taper_populus_tremuloides(dbh, H, h)

        self.assertEqual(round(d_2, 2), 5.78, f"diameter at half height is wrong")

    def test_taper_swedish(self):
        """test the diameter calculation"""
        tve = self.tve["data"][0]
        H = tve["h"]
        h = H / 2
        dbh = tve["dbh"]

        d_2 = TreeVolumePredictor.taper_spangberg(H=H, h=h, dbub=dbh)

        self.assertEqual(round(d_2, 2), 18.44, f"diameter at half height is wrong")

    def test_huber(self):
        """test huber equation by first estimating the mid height diameter with a taper function"""
        tve = self.tve["data"][0]
        H = tve["h"]
        h = H / 2

        # if centimeter goes in, centimeter goes out
        dbh = tve["dbh"]
        d_2 = TreeVolumePredictor.taper_spangberg(H=H, h=h, dbub=dbh)
        d_2 = d_2 / 100  # get it to meter
        v_actual = TreeVolumePredictor.huber(d_2, H)
        v_expected = tve["v"] / 1000
        abs_percent = abs(1 - v_actual / v_expected)

        self.assertLessEqual(
            abs_percent,
            0.20,
            f" calculated volume: {v_actual} and expteced volume: {tve['v']}",
        )

    def test_denzin(self):
        """
        test denzin formula
        """
        tve = self.tve["data"][0]
        h = tve["h"]
        dbh = tve["dbh"]
        v_expected = tve.get("v")
        v_expected /= 1000  # the data provided by gutten is in cubic centimeters
        v_actual = TreeVolumePredictor.denzin(dbh, h)
        ape = self.absolute_percent_error(v_actual, v_expected)

        # we are expecting less then 30 percent difference from the predicted volume to the real volume
        self.assertLessEqual(ape, 0.3, f"the absolute percentage error is {ape}")

    def test_denzin_gutten(self):
        """
        test denzin formula with guttenberg 1915 data
        """
        gutten_df = self.dh.get_gutten_df()
        gutten_df["v_actual"] = gutten_df.apply(
            lambda x: TreeVolumePredictor.denzin(x["dbh.cm"], x["height"]) * 1000,
            axis=1,
        )

        gutten_df["abs_difference"] = gutten_df["v_actual"] - (gutten_df["volume"])
        gutten_df["ape"] = self.absolute_percent_error(
            gutten_df["v_actual"], gutten_df["volume"]
        )

        self.assertLessEqual(
            gutten_df["ape"].mean(),
            0.25,
            f"the mean absolute percentage error is too high",
        )

    def test_denzin_schepaschenko(self):
        """
        test denzin formula with schepaschenka data
        """
        df_treedb = self.dh.get_schepaschenko2017_df()["Tree_db"]
        df_treedb.columns = df_treedb.columns.str.replace(" ", "_").str.lower()

        df_treedb["v_actual"] = df_treedb.apply(
            lambda x: TreeVolumePredictor.denzin(x["dbh"], x["h_tree"]) * 1000, axis=1
        )

        df_treedb["abs_difference"] = df_treedb["v_actual"] - (df_treedb["vtot"])
        df_treedb["ape"] = self.absolute_percent_error(
            df_treedb["v_actual"], df_treedb["vtot"]
        )

        df_treedb = df_treedb[df_treedb["ape"] > 0]
        ape = df_treedb["ape"].mean()
        self.assertLessEqual(
            ape, 0.25, f"the mean absolute percentage error is too high. It is {ape}"
        )

    def test_zianis_eq_86(self):
        """test Zianis equation 86"""

        tve = self.tve["data"][0]
        h = tve["h"]
        dbh = tve["dbh"]
        v_expected = tve.get("v")
        v_expected /= 1000  # the data provided by gutten is in cubic centimeters
        v_actual = TreeVolumePredictor.zianis_eq_86(H=h, D=dbh)
        ape = self.absolute_percent_error(v_actual, v_expected)

        # we are expecting less then 30 percent difference from the predicted volume to the real volume
        self.assertLessEqual(ape, 0.3, f"the absolute percentage error is {ape}")

    def test_zianis_eq_94(self):
        """test Zianis equation 94"""

        tve = self.tve["data"][0]
        h = tve["h"]
        dbh = tve["dbh"] / 100  # get to meters
        v_expected = tve.get("v")
        v_expected /= 1000  # the data provided by gutten is in cubic centimeters
        v_actual = TreeVolumePredictor.zianis_eq_94(H=h, D=dbh)
        ape = self.absolute_percent_error(v_actual, v_expected)

        # we are expecting less then 30 percent difference from the predicted volume to the real volume
        self.assertLessEqual(ape, 0.3, f"the absolute percentage error is {ape}")

    def test_zianis_eq_102(self):
        """test Zianis equation 102"""
        tve = self.tve["data"][0]
        H = tve["h"]

        dbh = tve["dbh"]
        v = tve["v"] / 1000
        v_actual = TreeVolumePredictor.zianis_eq_102(D=dbh, H=H)
        abs_percent = abs(1 - v_actual / v)

        # the calculation of the volume
        self.assertLessEqual(
            abs_percent,
            0.10,
            f" calculated volume: {v_actual} and expected volume: {tve['v']}",
        )

    def test_zianis_eq_170(self):
        """test Zianis equation 170"""
        tve = self.tve["data"][0]
        H = tve["h"]

        # if centimeter goes in, centimeter goes out
        dbh = tve["dbh"]
        v = tve["v"] / 1000
        v_actual = TreeVolumePredictor.zianis_eq_170(D=dbh, H=H)
        abs_percent = abs(1 - v_actual / v)

        # the calculation of the volume
        self.assertLessEqual(
            abs_percent,
            0.20,
            f" calculated volume: {v_actual} and expected volume: {tve['v']}",
        )

    def test_zianis_eq_195(self):
        """test Zianis equation 195"""
        tve = self.tve["data"][0]
        H = tve["h"]

        dbh = tve["dbh"]
        v = tve["v"] / 1000  # cubic decimeter to cubic meter
        v_actual = TreeVolumePredictor.zianis_eq_195(D=dbh, H=H)
        abs_percent = abs(1 - v_actual / v)

        # the calculation of the volume
        self.assertLessEqual(
            abs_percent,
            0.15,
            f" calculated volume: {v_actual} and expected volume: {tve['v']}",
        )

    def absolute_error(self, x_i, x):
        """
        calculate the absolute difference

        :param x_i: first value
        :param x: second value
        :return: absolute difference
        """
        return abs(x_i - x)

    def absolute_percent_error(self, x_i, x):
        """
        calculate the absolute percent error as error metric
        :param x_i:
        :param x:
        :return:
        """
        ae = self.absolute_error(x_i, x)
        return ae / x


if __name__ == "__main__":
    unittest.main()
