"""
runs some dendrometric functions to generate a report based on schepaschenko and guttenberg data

@author: Christian Winkelmann
"""

import json
from pathlib import Path

import pandas as pd
from loguru import logger

from treevolume.functions import DataHelper, TreeVolumePredictor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error


def prepare_schepaschenko_data(species="Pinus sylvestris L.") -> pd.DataFrame:
    """
    loads the data https://doi.org/10.1594/PANGAEA.871465 and https://doi.org/10.1594/PANGAEA.871491
    :param: species
    :return:
    """
    logger.info(
        "evaluate Schepaschenko measurements with the volume estimator functions."
    )
    dh = DataHelper()
    df_t = dh.get_schepaschenko2017_df()["Tree_db"]
    df_t.columns = df_t.columns.str.replace(" ", "_").str.lower()
    ## filter for the right species and omit fields which we don't want
    df_t = df_t[df_t.species.str.contains(species)][
        ["species", "dbh", "h_tree", "vtot"]
    ]
    df_t = df_t[
        df_t["dbh"].notnull() & df_t["h_tree"].notnull() & df_t["vtot"].notnull()
    ]

    return df_t


def prepare_gutten_data() -> pd.DataFrame:
    """

    :return:
    """
    dh = DataHelper()
    gutten_df = dh.get_gutten_df()
    gutten_df = gutten_df[["dbh.cm", "height", "volume"]]
    gutten_df["species"] = "Picea Abies"
    return gutten_df


def compare_functions(
    df_t: pd.DataFrame, diameter_fn: str, height_fn: str, vol_fn: str, species: str
) -> dict:
    """
    run multiple tree volume calculation functions

    :param df_t: Dataframe containing fields diameter_fn, height_fn
    :param diameter_fn: fieldname for diameter
    :param height_fn: fieldname for height
    :param vol_fn: fieldname for volume
    :param species: species name
    :return:
    """
    logger.info("calculate volume for Pinus Sylvestris using Denzin equation")
    df_t["v_denzin"] = df_t.apply(
        lambda x: TreeVolumePredictor.denzin(dbh=x[diameter_fn], h=x[height_fn]) * 1000,
        axis=1,
    )

    if species == "Pinus sylvestris L.":
        logger.info(
            "calculate volume for Pinus Sylvestris using equation 170 from zianis et. al "
        )
        df_t["v_zianis_eq"] = df_t.apply(
            lambda x: TreeVolumePredictor.zianis_eq_170(
                H=x[height_fn], D=x[diameter_fn]
            )
            * 1000,
            axis=1,
        )

    elif species == "Picea abies":
        logger.info(
            "calculate volume for Picea Abies using equation 102 from zianis et. al "
        )
        df_t["v_zianis_eq"] = df_t.apply(
            lambda x: TreeVolumePredictor.zianis_eq_102(
                H=x[height_fn], D=x[diameter_fn]
            )
            * 1000,
            axis=1,
        )
    else:
        raise ValueError("wrong species")

    df_t["d_2"] = df_t.apply(
        lambda x: TreeVolumePredictor.taper_spangberg(
            dbub=x[diameter_fn], h=x[height_fn] / 2, H=x[height_fn], species=species
        ),
        axis=1,
    )

    logger.info("calculate volume for Pinus Sylvestris using huber equation ")
    df_t["v_huber"] = df_t.apply(
        lambda x: TreeVolumePredictor.huber(d_2=x["d_2"] / 100, h=x[height_fn]) * 1000,
        axis=1,
    )

    rmse_huber = mean_squared_error(df_t[vol_fn], df_t["v_huber"], squared=False)
    mape_huber = mean_absolute_percentage_error(df_t[vol_fn], df_t["v_huber"])
    logger.info(f"RMSE for huber equation: {rmse_huber}. MAPE: {mape_huber}")

    rmse_eq = mean_squared_error(df_t[vol_fn], df_t["v_zianis_eq"], squared=False)
    mape_eq = mean_absolute_percentage_error(df_t[vol_fn], df_t["v_zianis_eq"])
    logger.info(f"RMSE for zianis equation: {rmse_eq}. MAPE: {mape_eq}")

    rmse_denzin = mean_squared_error(df_t[vol_fn], df_t["v_denzin"], squared=False)
    mape_denzin = mean_absolute_percentage_error(df_t[vol_fn], df_t["v_denzin"])
    logger.info(f"RMSE for Denzin formula: {rmse_denzin}. MAPE: {mape_denzin}")

    return {
        "denzin": {"rmse": rmse_denzin, "mape": mape_denzin},
        "huber": {"rmse": rmse_huber, "mape": mape_huber},
        "zianis": {"rmse": rmse_eq, "mape": mape_eq},
        "n": len(df_t)
        # "modified_df": df_t.to_dict(orient='rows')
    }


def whole_analysis(output_filename=None):
    """
    demo functions which does all comparisons and outputs json data dump
    :return:
    :param output_filename:
    """
    result = {}
    species = "Picea abies"
    df_t = prepare_schepaschenko_data(species=species)
    result[f"schepaschenko_{species}"] = compare_functions(
        df_t=df_t, height_fn="h_tree", diameter_fn="dbh", vol_fn="vtot", species=species
    )

    df_t = prepare_gutten_data()
    result[f"gutten_{species}"] = compare_functions(
        df_t=df_t,
        height_fn="height",
        diameter_fn="dbh.cm",
        vol_fn="volume",
        species=species,
    )

    species = "Pinus sylvestris L."
    df_t = prepare_schepaschenko_data(species=species)
    result[f"schepaschenko_{species}"] = compare_functions(
        df_t=df_t, height_fn="h_tree", diameter_fn="dbh", vol_fn="vtot", species=species
    )

    logger.info(f"result of comparisons: {result}")

    if output_filename is not None:
        with (open(output_filename, "w+")) as f:
            logger.info(f"write result to disk path: {output_filename}")
            json.dump(result, f)


if __name__ == "__main__":
    here = Path(__file__).parent.resolve()
    whole_analysis(output_filename=here.joinpath("treevolume_analysis.json"))
