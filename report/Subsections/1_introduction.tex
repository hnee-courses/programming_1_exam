



\section{Introduction}
Estimation of tree stem volume is necessary for economical and ecological motivations. Timber is one of the most important resources from forests \parencite{Duncker2012} and the amount of stored carbon in trees is needed for carbon accounting \parencite{Meason2020}. To get reliable stock estimates precise modelling functions are needed. 


\subsection{Exact Measurement of Tree Stem Volume}
Measuring the volume of a tree by water displacement is the simplest \textcite{Hughes2005,Berendt2021}. But as it requires cutting down the tree it is impractical when a tree is huge and is not suitable if the value of a standing tree is needed. 
The second option of obtaining exact measurements is \ac{TLS} \parencite{Panagiotidis2021}. But handheld laserscanner are expensive and a dataset for comparisons was not obtainable.
In the following the focus is on estimations of tree volumes and not exact measurements.

\subsection{Estimation of Tree Stem Volume by Time}
One way is to estimate tree volume as a function of its age. Growth of tree is not linear and depends on environmental factors \parencite{Paine2012}.
Theses influences the radial and longitudinal growth \parencite{podor2014}. Biotic and abiotic factors temperature, precipitation, soil quality, hours of sunlight \textcite{biologyonline2022} and most important the species itself influence tree growth \parencite{Zeide1993}. Indicators exist that growth pattern have changed over the last decades \parencite{Kangas2020}.



\subsection{Estimation of Tree Stem Volume by Dimensions}
The volume of a tree stem is described by its dimensions. Historically the most measured attributes of trees are \acf{dbh} and \acf{h:tree:tot} \parencite{zianis2005, salekin2021}.
In economical contexts \acf{dbh_ub} and \acf{v_m} are used to calculate the volume of timber without the bark \parencite{Berendt2021, robertson1993}.

\subsubsection{Simple Geometry}
A tree could be assumed to be a cylinder, with a volume calculated by its \ac{dbh} and \ac{h:tree:tot}

\subsubsection{Sectional Models}
Sectional model approximate trees by putting them into segments of linear shape like in Figure \ref{fig:tree_taper}. In the simplest form the Huber Equation in \ref{eq:huber} uses the stem diameter at the middle of the stem ($S_\frac{1}{2}$) and the basal area ($A$) \parencite{CruzdeLeon2013}. This assumes the missing volume at the top is compensated by the wider stem at the bottom.

\begin{equation}\label{eq:huber}
	v = A * S_\frac{1}{2}
\end{equation}


\subsubsection{Tree Shape Estimation}
A tree has a more complex shape than a cylinder. The Denzin Formula in equation \ref{eq:denzin} assumes a constant decrease of diameter of 3\% per meter.

In equation \ref{eq:denzin} $dbh$ refers to \acf{dbh}, $h$ to \acf{h:tree:tot} and $v$ to \acf{v_tot}. The denzin formula models a linear reduction of the tree diameter with increasing height \parencite{graves1906}.
\begin{equation}\label{eq:denzin}
	v = \frac{d_bh ^ 2}{1000} (1 + 0.003 (h-30)
\end{equation}
More complex tree shape estimations can be done using taper functions which approximates tree shapes like in Figure \ref{fig:tree_taper}. There are three types of taper functions: single, segmented and variable-form taper functions \parencite{Rojo2005, Kozak1969, salekin2021}.


\begin{figure}
\centering
\includegraphics[width=8cm]{figures/TreeTaper.png}
\caption{Tree Taper Example \parencite{robertson1993}}
\label{fig:tree_taper}
\end{figure}




For each tree species an individual taper function can be defined. For equation \ref{eq:Spangberg2001} the diameter at any height can be estimated with for Picea Abies and for Pinus Sylvestris.

\begin{table}[H]
\centering
\caption{ Variables for \textcite{spangberg2001}  }

    \begin{tabular}{l|l | l }
    Variable & Picea Abies & Pinus Sylvestris \\
    \hline
    \hline
    $b_1$ & -3.8158 & -5.6198 \\
    $b_2$ & 1.8128 &  2.7310 \\
    $b_3$ & -2.0333 & -3.1194 \\
    $b_4$ & 254.4 & 141.9 \\
    $a_1$ & 0.6778 & 0.7611 \\
    $a_2$ & 0.0677 & 0.0757 \\
    x & \multicolumn{2}{c}{$ \frac{h}{H}$}
    \end{tabular}
    \label{tab:taper_functions compare}
\end{table}


\[
    I_1 = 
\begin{cases}
    1, & \text{if } x\leq a_1\\
    0,              & \text{otherwise}
\end{cases}
\]
\[
    I_2 = 
\begin{cases}
    1, & \text{if } x\leq a_2\\
    0,              & \text{otherwise}
\end{cases}
\]

\begin{equation}\label{eq:Spangberg2001}
	d_2 = \sqrt{ dbub^2 (  (b1 \cdot (x-1) + b2 \cdot(x^2-1) + b3 \cdot(a1-x)^2 I1 + b4 \cdot(a2-x)^2  \cdot I2 )   ) }
\end{equation}


\begin{table}[H]
\centering
\caption{Example Volume Functions for Tree Species from Appendix B, \textcite{zianis2005} }

    \begin{tabular}{l||l l l}
    tree species & volume function \\
    \hline
    \hline
    102 Norway Spruce (Picea abies)  & $0.6844  \cdot H^{3.0296}  \cdot D^{2.0560} (H - 1.3)^{-1.7377}  \cdot ( D+40)^{-0.9756}$  \\
    170 Scots pine (Pinus sylvestris) & $0.1028  \cdot D^2+0.02705  \cdot D^2 H+0.005215  \cdot D  \cdot H^2$  \\
    195 Douglas Fir (Pseudotsuga menziesii) & $ 1.8211  \cdot H^{4.153} D^{2.1342}  \cdot (H - 1.3)^{-2.6902} \cdot(D+40)^{–1.4265}$ \\

    \end{tabular}
    \label{tab:volume_functions compare}
\end{table}






